<?php

namespace Drupal\hal_collection\Form;

use Drupal\i3s_commons\SettingsCommons;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class HalCollectionSettingsForm extends ConfigFormBase
{

    public function getFormId()
    {
        return 'hal_collection_settings_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form = parent::buildForm($form, $form_state);
        $config = $this->config('hal_collection.settings');

        $form['hal_collection_settings']['collection'] = [
            '#type' => 'details',
            '#title' => $this->t("Collection"),
            '#open' => true,
        ];
        $form['hal_collection_settings']['collection']['code'] = [
            '#type' => 'textfield',
            '#title' => $this->t("Collection code:"),
            '#description' => $this->t("HAL code of the collection."),
            '#default_value' => $config->get('hal_collection.code'),
            '#required' => true,
        ];
        $form['hal_collection_settings']['collection']['base_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t("Base URL:"),
            '#description' => $this->t("Collection webpage base URL."),
            '#default_value' => $config->get('hal_collection.base_url'),
        ];

        $form['hal_collection_settings']['misc'] = [
            '#type' => 'details',
            '#title' => $this->t("Miscellaneous"),
            '#open' => false,
        ];
        $form['hal_collection_settings']['misc']['number_of_publications'] = [
            '#type' => 'number',
            '#title' => $this->t("Number of publications:"),
            '#description' => $this->t("Number of publications to display among the latest."),
            '#default_value' => $config->get('hal_collection.number_of_publications'),
            '#required' => true,
        ];
        $form['hal_collection_settings']['misc']['display_badges'] = [
            '#type' => 'checkbox',
            '#title' => $this->t("Attribute display"),
            '#description' => $this->t("Check to display attributes (audience, peer reviewing etc.) as “badges”."),
            '#default_value' => $config->get('hal_collection.display_badges'),
        ];
        $form['hal_collection_settings']['misc']['menu_weight'] = [
            '#type' => 'number',
            '#title' => $this->t("Menu item “weight”:"),
            '#description' => $this->t("Menu item display order (the smallest value is displayed first)."),
            '#default_value' => $config->get('hal_collection.menu_weight'),
            '#required' => true,
        ];

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $commons = new SettingsCommons();
        $config = $this->config('hal_collection.settings');

        $config->set('hal_collection.code', strtoupper(trim($form_state->getValue('code'))));
        $config->set('hal_collection.base_url', $commons->setProperUrlEnding($form_state->getValue('base_url')));

        $config->set('hal_collection.number_of_publications', $form_state->getValue('number_of_publications'));
        $config->set('hal_collection.display_badges', $form_state->getValue('display_badges'));
        $config->set('hal_collection.menu_weight', $form_state->getValue('menu_weight'));

        $config->save();

        \Drupal::service('router.builder')->rebuild();

        return parent::submitForm($form, $form_state);
    }

    protected function getEditableConfigNames()
    {
        return [
          'hal_collection.settings',
        ];
    }
}
