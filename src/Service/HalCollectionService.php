<?php

namespace Drupal\hal_collection\Service;

use Drupal\i3s_commons\CurlCommons;

class HalCollectionService
{
    private $curlCommons;
    private $collectionSettings;

    public function __construct()
    {
        $this->curlCommons = new CurlCommons();
        $this->halSettings = \Drupal::config('i3s_commons.settings');
        $this->collectionSettings = \Drupal::config('hal_collection.settings');
    }

    public function getCollectionLastPublications(): ?array
    {
        $response = $this->curlCommons->getHalData(sprintf(
            '%1$s%2$s/?wt=json&instance_s=hal&q=*:*&rows=%3$s&fl=%4$s&sort=%5$s',
            $this->halSettings->get('i3s_commons.search_url'),
            $this->collectionSettings->get('hal_collection.code'),
            $this->collectionSettings->get('hal_collection.number_of_publications'),
            'publicationDateY_i,authFullName_s,title_s,citationRef_s,docType_s,audience_s,peerReviewing_s,popularLevel_s,proceedings_s,invitedCommunications_s,uri_s,halId_s',
            'publicationDate_tdate+desc'
        ));

        if ($response !== null) {
            return $response['response']['docs'];
        }

        return null;
    }
}
