<?php

namespace Drupal\hal_collection\Controller;

use Drupal\i3s_commons\HalCommons;
use Drupal\hal_collection\Service\HalCollectionService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HalCollectionController extends ControllerBase
{
    private $halCommons;
    private $halService;
    private $collectionSettings;
    private $modulePath;
    private $twig;

    public function __construct(HalCollectionService $halService, TwigEnvironment $twig)
    {
        $this->twig = $twig;
        $this->halService = $halService;
        $this->halCommons = new HalCommons();
        $this->modulePath = drupal_get_path('module', 'hal_collection');
        $this->collectionSettings = \Drupal::config('hal_collection.settings');
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('hal_collection.service'),
            $container->get('twig'),
        );
    }

    public function lastPublications(): ?array
    {
        $results = $this->halService->getCollectionLastPublications();

        if (empty($results)) {
            $papers = $results;
        } else {
            $papers = [];
            foreach ($results as $result) {
                $paper = [];
                $this->halCommons->setPaperCommonData(
                    $paper,
                    $result,
                    $this->collectionSettings->get('hal_collection.display_badges')
                );
                $paper['type'] = $this->halCommons->getDocTypeLabel($result['docType_s'], 1);
                $papers[] = $paper;
            }
        }

        $collectionCode = $this->collectionSettings->get('hal_collection.code');
        $collectionBaseUrl = $this->collectionSettings->get('hal_collection.base_url');

        $template = $this->twig->loadTemplate(
            $this->modulePath . '/templates/base.html.twig'
        );

        return [
            '#type' => 'markup',
            '#markup' => $template->render([
                'papers' => $papers,
                'collectionCode' => $collectionCode,
                'collectionBaseUrl' => $collectionBaseUrl,
            ]),
            '#attached' => [
                'library' => [
                    'i3s_commons/hal-papers',
                ],
            ]
        ];
    }
}
